/******************
	This test provies scenario for smoke testing a NSI-WS:
		1.- smoke test provides a sanity check every time there are new changes to the NSI-WS.
		2.- Verify that your system doesn't throw any errors when under minimal load.
	
*******************/
import http from 'k6/http';
import { check, sleep, group, fail } from 'k6';

//Default NSI-WS host url
let NSIWS_HOSTNAME = "http://127.0.0.1:81";
if(typeof __ENV.NSIWS_HOSTNAME !== 'undefined'){
	NSIWS_HOSTNAME = __ENV.NSIWS_HOSTNAME;
}
	
let PID = 3497345;
if(typeof __ENV.K6_PID !== 'undefined'){
	PID = __ENV.K6_PID;
}

let TEST_NAME = "First-n-tests";
if(typeof __ENV.TEST_NAME !== 'undefined'){
	TEST_NAME = __ENV.TEST_NAME;
}

let INPUT_FILE = "./Resources/test-cases-firstN.json";
if(typeof __ENV.TEST_CASES_FILE !== 'undefined'){
	INPUT_FILE = __ENV.TEST_CASES_FILE;
}

//Load test cases from json file
const TEST_CASES= JSON.parse(open(INPUT_FILE));
		
//NSI-WS available data response types
//const SUPPORTED_RESPONSE_FORMATS =["csv", "json", "xml"];
const SUPPORTED_RESPONSE_FORMATS =["csv"];
	
export let options = {
	ext: {
		loadimpact: {
		  projectID: PID, //k6 CLOUD project id 
		  name: TEST_NAME
		}
	},

	scenarios: {
		contacts: {
			executor: 'shared-iterations',
			vus: 1,
			iterations: 5, //50,
			maxDuration: '10h',
		},
	},	
	//Base line test 
	//Fixed number of iterations to execute the default function.
	//iterations: 2, //25,
	//vus: 1,  // 1 user looping 
	//duration: '1m',
	thresholds: {
		'checks': ['rate>0.99'], // more than 99% success rate
	
        "http_req_duration{group:::Query type data::Format csv::Size flat}": 	["avg<5000"],
        "http_req_duration{group:::Query type data::Format csv::Size series}": 		["avg<5000"],
	
        "http_req_duration{group:::Query type data::Format csv::Size constrainted}": 	["avg<5000"],
        "http_req_duration{group:::Query type data::Format csv::Size constrainted_paginated}": 		["avg<5000"],
        "http_req_duration{group:::Query type data::Format csv::Size non-constrainted}": 		["avg<5000"],
        "http_req_duration{group:::Query type data::Format csv::Size non-constrainted_paginated}": 		["avg<5000"],
        "http_req_duration{group:::Query type data::Format csv::Size small_non-constrainted}": 		["avg<5000"],
        "http_req_duration{group:::Query type data::Format csv::Size small_non-constrainted_paginated}": 		["avg<5000"],
        "http_req_duration{group:::Query type data::Format csv::Size control_query}": 		["avg<5000"],
        "http_req_duration{group:::Query type data::Format csv::Size small_control_query}": 		["avg<5000"],
		
        "http_req_duration{datasetSize:constrainted}":["avg<5000"],
        "http_req_duration{datasetSize:constrainted_paginated}":     ["avg<5000"],
        "http_req_duration{datasetSize:non-constrainted}": 	  ["avg<5000"],
        "http_req_duration{datasetSize:non-constrainted_paginated}": 	  ["avg<5000"],
        "http_req_duration{datasetSize:small_non-constrainted}": 	  ["avg<5000"],
        "http_req_duration{datasetSize:small_non-constrainted_paginated}": 	  ["avg<5000"],
		
		"http_req_duration{datasetSize:control_query}": 	  ["avg<5000"],
		"http_req_duration{datasetSize:small_control_query}": 	  ["avg<5000"]
		
	},	
	//Discard the response bodies to lessen the amount of memmory required by the testing machine.
	discardResponseBodies: true,

};

export function setup() {
	// 2. setup code
		
	//check that the nsi-ws hostname is available
	let healhCheck = http.get(`${NSIWS_HOSTNAME}/health`);
	if (healhCheck.status !== 200){
		fail(`Error: the NSI-WS {${NSIWS_HOSTNAME}/health} is not responding.`);
	}
	
	console.log(`Testing the NSI-WS {${NSIWS_HOSTNAME}}`);
}

export default function() {
	//Iterate throu
	for(let testCase in TEST_CASES){
		//Generate random query 
		let testQuery = TEST_CASES[testCase];
		//console.log('Query: ', testQuery.query);
		
		let params = { headers: {'Accept-Ecoding': 'gzip, deflate', 'Content-Type':'application/xml'}};
		
		group(`Query type ${testQuery.queryType}`, function (){
			
			//structure query			
			if(testQuery.queryType==="structure"){
				params = { headers: {'Accept-Ecoding': 'gzip, deflate', 'Content-Type':'application/vnd.sdmx.structure+json; version=1.0; charset=utf-8'}};
				group(`Struc type ${testQuery.structureType}`, function (){
					let response = http.get(
						`${NSIWS_HOSTNAME}${testQuery.query}`, //URL
						params, //headers 
					);	
								
					check(response, {
						"status is 200": (r) => r.status == 200
					});
					//console.log(response.headers["Content-Type"]);
				});
			}
				
			//data query
			else{
				for(let format in SUPPORTED_RESPONSE_FORMATS){
					//Generate random format 
					let responseFormat = SUPPORTED_RESPONSE_FORMATS[format];
					if(typeof __ENV.RESPONSE_FORMAT !== 'undefined')
						responseFormat = __ENV.RESPONSE_FORMAT;
					//console.log('Format: ', responseFormat);
					
					params.headers.Accept=`application/vnd.sdmx.data+${responseFormat}`;
					if(responseFormat==="xml")
						params.headers.Accept=`application/vnd.sdmx.genericdata+${responseFormat}`;
					
					var expectedResponseStatusCode = 200;
					//has range headers
					if ('range' in testQuery) {
						params.headers.Range=testQuery.range;
						expectedResponseStatusCode = 206;//partial content
					}
					//add tags
					params.tags={'datasetSize':testQuery.datasetSize}
					
					params.timeout='1200s';
					
					group(`Format ${responseFormat}`, function (){
						group(`Size ${testQuery.responseSize}`, function (){
							let response = http.get(
								`${NSIWS_HOSTNAME}${testQuery.query}`, //URL
								params, //headers 
							);	
							var msg= `status is ${expectedResponseStatusCode}`;
							check(response, {
								msg: (r) => r.status == expectedResponseStatusCode
							});
							//console.log(response.headers["Content-Type"]);
							//console.log(Object.keys(response.headers));
						});
					});
					//wait 1 second after each test iteration
					sleep(10);
				}
			}
		});
	}
}
