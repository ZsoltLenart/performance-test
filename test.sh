#!/bin/bash 

docker run --name FirstNLarge -e TEST_CASES_FILE="./Resources/test-cases-firstN.json"  --network="host" -e NSIWS_HOSTNAME="http://localhost:80" -v "/home/dotstat/git/performance-test/Resources:/Resources/" -i loadimpact/k6 run - < first-n-queries-large.js
docker rm FirstNLarge
